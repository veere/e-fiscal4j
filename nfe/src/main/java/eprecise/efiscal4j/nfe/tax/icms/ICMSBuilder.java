
package eprecise.efiscal4j.nfe.tax.icms;

interface ICMSBuilder {

    ICMS build();

}
