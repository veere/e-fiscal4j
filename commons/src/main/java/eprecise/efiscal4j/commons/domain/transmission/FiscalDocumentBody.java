
package eprecise.efiscal4j.commons.domain.transmission;



public abstract class FiscalDocumentBody {

    public abstract TransmissibleBodyImpl getTransmissible();

}
