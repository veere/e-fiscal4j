
package eprecise.efiscal4j.nfe;

import eprecise.efiscal4j.nfe.domain.NFeDomain;


public interface Testable {

    NFeDomain getTestDomain();

}
