package eprecise.efiscal4j.nfe.tax.pis;

public interface PISBuilder {
    PIS build();
}
